﻿using Android.App;
using Android.OS;

namespace NextGenCanMobile
{
    [Activity(Label = "MostWasteList")]
    public class MostWasteList : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.MostWasteList);
        }
    }
}