﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using NextGenCanMobile.Logic;

namespace NextGenCanMobile
{
    [Activity(Label = "GroceryList")]
    public class GroceryList : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.GroceryList);

            var btnProduct = FindViewById<Button>(Resource.Id.btnProduct);
            btnProduct.Click += (sender, args) =>
            {
                var nextActivity5 = new Intent(this, typeof(AddProduct));
                StartActivity(nextActivity5);
            };
        }
    }
}