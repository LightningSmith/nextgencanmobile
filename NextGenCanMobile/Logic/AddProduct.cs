﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace NextGenCanMobile.Logic
{
    [Activity(Label = "AddProduct")]
    public class AddProduct : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.AddProduct);

            var btnAdd = FindViewById<Button>(Resource.Id.btnAdd);
            btnAdd.Click += (sender, args) =>
            {
                var nextActivity6 = new Intent(this, typeof(GroceryList));
                StartActivity(nextActivity6);
            };
    }
}

}