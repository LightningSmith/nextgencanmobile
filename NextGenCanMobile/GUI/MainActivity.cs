﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Gr.Net.MaroulisLib;
using NextGenCanMobile.Logic;
//using Xamarin.Essentials;
using Xamarin.Forms.Platform.Android;

namespace NextGenCanMobile
{
    [Activity(Label = "GenCan", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        public LinearLayout layout;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
           // Platform.Init(this, savedInstanceState);

            // Show SplashScreen
            var config = new EasySplashScreen(this)
                .WithFullScreen()
                .WithTargetActivity(Java.Lang.Class.FromType(typeof(Navigation)))
                .WithSplashTimeOut(5000) //5 seconds
                .WithBackgroundColor(Color.ParseColor("#ffffff"))
                .WithLogo(Resource.Drawable.gclogo2)
                .WithHeaderText("")
                .WithFooterText("Copyright 2019-2020")
                .WithBeforeLogoText("")
                .WithAfterLogoText("GenCan");

            //Set TextColor
            config.HeaderTextView.SetTextColor(Color.Black);
            config.FooterTextView.SetTextColor(Color.Black);
            config.BeforeLogoTextView.SetTextColor(Color.Black);
            config.AfterLogoTextView.SetTextColor(Color.Black);

            //Create View
            View view = config.Create();

            //Set Content View
            SetContentView(view);



        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions,
            [GeneratedEnum] Permission[] grantResults)
        {
           // Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}