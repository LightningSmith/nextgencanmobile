﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace NextGenCanMobile
{
    [Activity(Label = "Navigation")]
    public class Navigation : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Navigation);


            var btnMostWaste = FindViewById<Button>(Resource.Id.btnMostWaste);
            btnMostWaste.Click += (sender, args) =>
            {
                var nextActivity = new Intent(this, typeof(MostWaste));
                StartActivity(nextActivity);
            };
            var btnGroceryList = FindViewById<Button>(Resource.Id.btnGroceryList);
            btnGroceryList.Click += (sender, args) =>
            {
                var nextActivity1 = new Intent(this, typeof(GroceryList));
                StartActivity(nextActivity1);
            };
            var btnTrashLevel = FindViewById<Button>(Resource.Id.btnTrashLevel);
            btnTrashLevel.Click += (sender, args) =>
            {
                var nextActivity2 = new Intent(this, typeof(TrashLevel));
                StartActivity(nextActivity2);
            };
        }
    }
}