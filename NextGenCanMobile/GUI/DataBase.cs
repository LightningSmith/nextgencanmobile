﻿using Android.App;
using Android.OS;
using MySql.Data.MySqlClient;

namespace NextGenCanMobile.DataBase
{
    [Activity(Label = "DataBase")]
    public class DataBase : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.DataBase);

        }
    }
}