﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace NextGenCanMobile
{
    [Activity(Label = "MostWaste")]
    public class MostWaste : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.MostWaste);

            var btnPieChart = FindViewById<Button>(Resource.Id.btnPieChart);
            btnPieChart.Click += (sender, args) =>
            {
                var nextActivity3 = new Intent(this, typeof(MostWastePieChart));
                StartActivity(nextActivity3);
            };
            var btnList = FindViewById<Button>(Resource.Id.btnList);
            btnList.Click += (sender, args) =>
            {
                var nextActivity4 = new Intent(this, typeof(MostWasteList));
                StartActivity(nextActivity4);
            };
        }
    }
}